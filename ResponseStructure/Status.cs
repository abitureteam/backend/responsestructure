﻿using Newtonsoft.Json;

namespace Abiture.ResponseStructure
{
    public sealed class Status : IStatus
    {
        public StatusCode Code { get; set; }
        int IStatus.Code => (int)Code;
        public string Message { get; set; }
        
    }
}