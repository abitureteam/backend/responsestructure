﻿namespace Abiture.ResponseStructure
{
    public sealed class ResponseWrapper<Payload> : IResponseWrapper<Payload>
    {
        public IStatus Status { get; set; }

        public Payload Data { get; set; }
    }
}