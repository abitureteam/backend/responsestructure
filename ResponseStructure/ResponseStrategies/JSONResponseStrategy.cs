﻿using Newtonsoft.Json;
using System;

namespace Abiture.ResponseStructure.ResponseStrategies
{
    public sealed class JSONResponseStrategy<Payload> : IResponseStrategy<Payload>
    {
        public string Convert(IResponseWrapper<Payload> wrappedData)
        {
            _ = wrappedData ?? throw new ArgumentNullException(nameof(wrappedData));
            return JsonConvert.SerializeObject(wrappedData);
        }
    }
}