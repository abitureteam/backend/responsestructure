﻿using System;
using YamlDotNet.Serialization;

namespace Abiture.ResponseStructure.ResponseStrategies
{
    public sealed class YAMLResponseStrategy<Payload> : IResponseStrategy<Payload>
    {
        public string Convert(IResponseWrapper<Payload> wrappedData)
        {
            _ = wrappedData ?? throw new ArgumentNullException(nameof(wrappedData));
            //Convert the used Status to a minimal Status. Otherwise the YAML Serializer will try to parse the Enum name
            var oldStatus = wrappedData.Status;
            wrappedData.Status = new StatusMinimal { Code = oldStatus.Code, Message = oldStatus.Message };
            return new SerializerBuilder().Build().Serialize(wrappedData);
        }
    }
}