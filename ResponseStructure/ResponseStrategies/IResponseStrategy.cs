﻿namespace Abiture.ResponseStructure.ResponseStrategies
{
    public interface IResponseStrategy<Payload>
    {
        string Convert(IResponseWrapper<Payload> wrappedData);
    }
}