﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abiture.ResponseStructure
{
    public class StatusMinimal : IStatus
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
