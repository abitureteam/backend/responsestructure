﻿namespace Abiture.ResponseStructure
{
    public interface IStatus
    {
        int Code { get; }
        string Message { get; }
    }
}