﻿namespace Abiture.ResponseStructure
{
    public interface IResponseWrapper<Payload>
    {
        IStatus Status { get; set; }
        Payload Data { get; set; }
    }
}