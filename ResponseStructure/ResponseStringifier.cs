﻿using Abiture.ResponseStructure.ResponseStrategies;

namespace Abiture.ResponseStructure
{
    public static class ResponseStringifier<Payload>
    {
        public static string Stringify(IResponseWrapper<Payload> responseWrapper, IResponseStrategy<Payload> strategy)
        {
            return strategy.Convert(responseWrapper);
        }
    }
}