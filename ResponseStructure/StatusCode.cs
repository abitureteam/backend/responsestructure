﻿namespace Abiture.ResponseStructure
{
    /// <summary>
    /// StatusCodes ARE NOT necessarily HTTP Codes.
    /// </summary>
    public enum StatusCode
    {
        /* 1XX INFO*/
        /* 2XX SUCCESS*/
        /* 3XX REDIRECT*/
        /* 4XX USER ERROR*/
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        MissingParameters = 450,
        InvalidParameters = 451,
        ResourceAlreadyExists = 480,
        /* 5XX SERVER ERROR*/
        ServerError = 500
    }
}