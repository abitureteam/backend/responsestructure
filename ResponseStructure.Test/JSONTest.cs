using Abiture.ResponseStructure.ResponseStrategies;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Abiture.ResponseStructure.Test
{
    public class JSON
    {

        [Test]
        public void ExpectNullToThrowException()
        {
            Assert.Throws(typeof(ArgumentNullException), () =>
            {
                string response = ResponseStringifier<TestItems.Item1>.Stringify(null, new JSONResponseStrategy<TestItems.Item1>());
            });
        }

        [Test]
        public void ExpectNullPayloadToNotThrowException()
        {
            Assert.DoesNotThrow(() =>
            {
                string response =
                    ResponseStringifier<TestItems.Item1>.
                        Stringify(new ResponseWrapper<TestItems.Item1>
                        {
                            Status = new Status { Message = "Unauthorized", Code = StatusCode.Unauthorized }
                        }
                        , new JSONResponseStrategy<TestItems.Item1>());
                Console.WriteLine(response);
            });
        }
        [Test]
        public void ExceptItem1ToContainAllMembers()
        {
            var item1 = new TestItems.Item1 { something = "asdf", a = 2, b = 1, c = 4f, d = 44f, name = "asdas", format = "json" };
            string toString = "";
            Assert.DoesNotThrow(() =>
            {
                toString = ResponseStringifier<TestItems.Item1>
                    .Stringify(
                        new ResponseWrapper<TestItems.Item1> 
                        { 
                            Data = item1, 
                            Status = new Status 
                            { 
                                Code = StatusCode.NotFound, 
                                Message = "Not found" 
                            } 
                        },
                        new JSONResponseStrategy<TestItems.Item1>()
                    );
                Console.WriteLine(toString);
            });
            dynamic fromString = JObject.Parse(toString);
            dynamic status = fromString.Status;
            dynamic data = fromString.Data;
            Assert.AreEqual(item1.a, (int)data.a);
            Assert.AreEqual(item1.b, (int)data.b);
            Assert.AreEqual(item1.c, (float)data.c);
            Assert.AreEqual(item1.d, (float)data.d);
            Assert.AreEqual(item1.format, (string)data.format);
            Assert.AreEqual(item1.name, (string)data.name);
            Assert.AreEqual(item1.something, (string)data.something);

            Assert.AreEqual((int)StatusCode.NotFound, (int)status.Code);
            Assert.AreEqual("Not found", (string)status.Message);

        }
        [Test]
        public void ExpectStatusCodeToEqualEnumStatusCode()
        {
            IStatus status = new Status { Code = StatusCode.Forbidden, Message = "Forbidden" };
            Assert.AreEqual((int)((Status)status).Code, status.Code);
        }
    }
}