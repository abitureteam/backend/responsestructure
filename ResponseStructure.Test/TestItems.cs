﻿namespace Abiture.ResponseStructure.Test
{
    public static class TestItems
    {
        public class Item1
        {
            public string name, format, something;
            public int a, b;
            public float c;
            public double d;
        }

        public class Item2
        {
            public string name;
            public Item1[] items;
        }

        public class Item3
        {
            public string name;
            public Item3 next;
        }
    }
}